import os
import sys
import csv
sys.path.append(os.getcwd())
from database.db import db, cursor
from mails import profesor_mail


def generate_files(): 

    info_lista = []    
    sql_fetch = ("SELECT student.ime, student.prezime, student.broj_indexa,  predmet.ime FROM student JOIN student_predmet sp ON student.student_id =  sp.student_id JOIN predmet  ON predmet.predmet_id = sp.predmet_id ORDER BY predmet.ime")
    cursor.execute(sql_fetch)
    rows = cursor.fetchall()
    db.commit()
    for row in rows:
        info_lista.append(row)

    lista_predmeta = []
    # ovaj list comprehension je nepotreban, mogao je da bude napravljen kad i info_lista gore

    info_student= [list(ele) for ele in info_lista]
  


           
    ime_predmeta = ""
    list_predmeta_prodjenih = []
    lista_studenata = []
    """ 
     Pazi na indentaciju, ovo je pravo cudo sto radi ovako. Bas lak nacin da dobijes greske koje su preteske za uociti.
     Ne ostavljati velike razmake izmedju linija koda, ponkekad je ok linija ili 2 ako je preglednije.
    """
    for i in range(len(info_student)):   # ne prolazi sa iteratorom, for row in info_student:
     
     ime_predmeta = info_student[i][3]  # deklarisana varijabla, i onda nije iskoristena u sledecoj liniji
     if info_student[i][3] in list_predmeta_prodjenih:
        
        lista_studenata.append(info_student[i])
        with open(f'./output/{info_student[i][3]}.csv', 'w', newline= '') as file:

            for row in lista_studenata:
             writer = csv.writer(file)
             writer.writerow(row)
     else: 
         
           
           

            with open(f'./output/{info_student[i][3]}.csv', 'w', newline= '') as file:
                for row in lista_studenata:
                 writer = csv.writer(file)
                 writer.writerow(row)
            lista_studenata = []
            lista_studenata.append(info_student[i])
            list_predmeta_prodjenih.append(ime_predmeta)
            with open(f'./output/{info_student[i][3]}.csv', 'w', newline= '') as file:
                for row in lista_studenata:
                 writer = csv.writer(file)
                 writer.writerow(row)




def mail_for_who():

    sql_profesor = (f"SELECT p.email, predmet.ime FROM profesor p JOIN predmet WHERE p.profesor_id = predmet.profesor_id")
    folder_path = './output/'
    files = [file for file in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, file))]
    svi_fajlovi = []

    """
    Ako je poenta ovog dijla koda da se dobiju svi predmeti iz upisa, bolji nacin je bio da se napise query koji vraca sve predmete iz upisa
    Ili da ovdje imamo vec kao argument proslijedjen iz funkcije generate_files.
    """

    for i in files:
       svi_fajlovi.append(i.replace('.csv', ''))
    cursor.execute(sql_profesor)
    rows = cursor.fetchall()
    db.commit()
    for row in rows:       
       if row[1] in svi_fajlovi:
          profesor_mail.send_mail(row[0], row[1])

"""
   Gdje se koristi Generate mail i zasto nema nikakav return ? 
"""

def generate_mail(email, ime_fajla):
   sql = (f"SELECT ime FROM profesor WHERE email = '{email}'")
   cursor.execute(sql)
   rows = cursor.fetchall()
   db.commit()
   print(f'Salje se mail na {email} sa fajlom {ime_fajla}')
 
   

"""
    Funkcija je nepotrbena ako samo sluzi za pozivanje druge funkcije. Imalo bi smisla da se doda makar neki error handling
    Ako pukne prva funkcija, ova druga nece ni biti pozvana, a korisnik nece dobiti nikakvu poruku o tome.
"""
def obavijesti_profesora():
   generate_files()
   mail_for_who()


















            
  
       
           
     




