from queries.sluzba_upis_query import find_upis_data, convert_to_bool, update_u_toku
from mails.sluzba_mail import send_mail
from profesor_obavestenje import obavijesti_profesora
import csv
import time
import re

"""
 Pravi 2 razmaka izmedju funkcija
"""


def save_into_csv(data_list_of_tuples, year):
    try:
        with open(f'output/upis_{year}.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(list(tup) for tup in data_list_of_tuples)
        f.close()
    except:
        print('Doslo je do greske prilikom cuvanja fajla!')
    else:
        print(f'Sacuvani podaci u upis_{year}.csv datoteku')


def sluzba_upis(email):
    while True:
        year = input('Izaberite skolsku godinu(1950-2024):\n')
        if re.match('^19[5-9]\d|20[0-5]\d$', year) is None:  # if not re.match(r'^19[5-9]\d|20[0-5]\d$', year):
            print('Netacan unos')
            continue

        coming_data = find_upis_data(year)
        if not len(coming_data):
            print(f'Nema informacija za {year} godinu.\nProbajte ponovo')
            continue
        # cuvanje podataka - pauza - slanje mejla
        save_into_csv(coming_data, year)
        time.sleep(2)
        send_mail(email, year)

        answer = input(f'Da li je upis za {year} zavrsen?\n')
        answer = answer.lower()
        while answer != 'da' and answer != 'ne':
            print('Pogresan unos')
            answer = input(f'Da li je upis za {year} zavrsen?\n')
            answer = answer.lower()

        if answer == 'da':
            update_u_toku(coming_data)
            print('Podaci su promjenjeni!')
            print()

        obavijesti_profesora()
        return
