import os
import sys
sys.path.append(os.getcwd())

from datetime import datetime
from database.db import db, cursor
from classes.student import Student
from classes.upis import Upis
from queries.student import get_godina_studija, get_student_from_jsk_query, get_student_id
from queries.student_predmet import get_nepolozeni_predmeti
from queries.predmet import get_predmeti_za_godinu_studija, get_predmet_from_id, get_id_from_name


'''
4. Napravite funkciju upisi_studenta. Nakon logina, student dobija listu
nepoloženih predmeta i predmeta za iduću godinu studija. Za ukupno 60 kredita
bira predmete koje će prijaviti (s tim što se odmah dodaju nepoloženi i
oduzima taj broj kredita). Printa se i želi li neke informacije o nekom predmetu
prije izbora. Nakon unosa imena predmeta, vraća se opis predmeta iz baze.
Ažuriraju se informacije o studentu u skladu s ovim i napravi se instanca klase Upis.
Ukoliko je student prenio preko 15 kredita, pozvati funkciju prenos.
'''


def upisi_studenta(student_jsk):

    # Iz jsk napravi objekat sa poljima kao klasa Student
    student_info = get_student_from_jsk_query(student_jsk)
    print(student_info)
    student = Student(False, student_info[1], student_info[2], student_info[3],
                    student_info[4], student_info[5], [], [], student_info[6], student_info[7])

    preostalo_kredita = 60

    nepolozeni_predmeti = [get_predmet_from_id(current_id) for current_id in get_nepolozeni_predmeti(student)]
    predmeti_za_iducu_godinu = get_predmeti_za_godinu_studija(get_godina_studija(student) + 1)

    # stampaj nepolozeni_predmeti i predmeti_za_iducu_godinu i broj kredita
    print("Nepoloženi predmeti - broj kredita:")
    for predmet in nepolozeni_predmeti:
        print(predmet['ime'], ' - ', predmet['broj_kredita'])
    print('---------------------------------------------------------------------')
    print("Predmeti za iduću godinu - broj kredita:")
    for predmet in predmeti_za_iducu_godinu:
        print(predmet['ime'], ' - ', predmet['broj_kredita'])

    # odmah u prijavljene dodaj nepolozene
    prijavljeni_predmeti = nepolozeni_predmeti.copy()

    # oduzmi broj kredita
    for predmet in nepolozeni_predmeti:
        preostalo_kredita -= predmet['broj_kredita']

    print(('\nUnesi "<ime predmeta> .info" za više informacija o predmetu.\n'
        'Unesi <ime predmeta> da prijaviš predmet.\n'
        'Unesi <-ime predmeta> da ukloniš predmet iz prijavljenih.\n'
        'Unesi "KRAJ" da završiš unos predmeta.\n'))
    
    while(True): # nepotrebne zagrade
        print('Preostalo kredita: ', preostalo_kredita)
        korisnicki_unos = input('>> ')

        if korisnicki_unos.upper() == 'KRAJ':
            break

        broj_kredita_predmeta = unos_predmeta_logika(korisnicki_unos, nepolozeni_predmeti,
                                                    predmeti_za_iducu_godinu, prijavljeni_predmeti, preostalo_kredita)
        preostalo_kredita -= broj_kredita_predmeta

    # Ažuriraju se informacije o studentu u skladu s ovim
    student.set_prijavljeni_predmeti(prijavljeni_predmeti.copy())
    # prijavljeni_predmeti_id = [get_id_from_name(predmet['ime']) for predmet in prijavljeni_predmeti]

    # Napravi se instanca klase Upis(student, skolska_godina, u_toku=True)
    novi_upis = Upis(get_student_id(student), datetime.now(), 1, novi_unos=False)

    # Ukoliko je student prenio preko 15 kredita, pozvati funkciju prenos.
    prenio_kredita = 0
    for predmet in nepolozeni_predmeti:
        prenio_kredita += predmet['broj_kredita']
    if prenio_kredita > 15:
        print('Prenos preko 15 kredita - promjena statusa u Samofinansiranje')
        prenos(student)
    else:
        print('Student nije prenio preko 15 kredita - ostaje na budžetu')


'''
5. Napraviti funkciju prenos koja za svakog studenta koji je prenio
preko 15 kredita pri upisu, mijenja status iz budžet na samofinansiranje
i balans postavlja na 0.
'''


def prenos(student):
    student.set_status('Samofinansiranje')
    student.set_balans(0)

def unos_predmeta_logika(korisnicki_unos, nepolozeni_predmeti, 
                        predmeti_za_iducu_godinu, prijavljeni_predmeti, preostalo_kredita):
    # Vraca broj kredita izabranog predmeta

    ime_predmeta = korisnicki_unos
    if korisnicki_unos[-5:] == '.info':  # ovo je ok, ali postoji metod endswith() da ne moras da borjis kakraktere
        ime_predmeta = korisnicki_unos[:-6]

        """ 
            Ovo je mogla biti jedna if petlja, trazis samo jedan predmet, ne moras da trazis element po element.
            Vidim da gledas da se izjenace u Uppercase, ali moglo je da se jednom transformise taj niz u uppercase
             i onda koristis samo if da prvjeris da li se predmet nalazi u toj listi.
            
            Ovjde su te liste kratke i nece uticati na performanse, ali kod nekih vecih lista, ovo moze biti problem.
        """
        for predmet in predmeti_za_iducu_godinu:
            if ime_predmeta.upper() == predmet['ime'].upper():
                print('Informacije o predmetu:')
                print('Ime:', predmet['ime'], '| Opis:', predmet['opis'], '| Broj kredita:',
                      predmet['broj_kredita'], '| Godina studija:', predmet['godina_studija'])
                return 0
   
        print('Predmet nije pronađen.')
        return 0
 
    elif korisnicki_unos[0] == '-':
        ime_predmeta = ime_predmeta[1:]
        for predmet in nepolozeni_predmeti:
            if ime_predmeta.upper() == predmet['ime'].upper():
                print('Ne može se ukloniti nepoloženi predmet.')
                return 0

        for predmet in prijavljeni_predmeti:
            if ime_predmeta.upper() == predmet['ime'].upper():
                # print("Prvi", prijavljeni_predmeti)
                prijavljeni_predmeti.remove(predmet)
                # print(prijavljeni_predmeti)
                print('Uklonjen predmet iz prijavljenih.')
                return -predmet['broj_kredita']
         
        print('Predmet nije prijavljen.')
        return 0
    else:
        # Nakon unosa imena predmeta, vraća se opis predmeta iz baze. (predmet.opis)
        for predmet in prijavljeni_predmeti:
            if ime_predmeta == predmet['ime']:
                print("Predmet je već izabran.")
                return 0
          
        for predmet in predmeti_za_iducu_godinu:
            if ime_predmeta == predmet['ime']:
                if preostalo_kredita < predmet['broj_kredita']:
                    print("Nemate dovoljno kredita da dodate predmet.")
                    return 0
              
                prijavljeni_predmeti.append(predmet)
                print("Predmet dodat, opis predmeta:", predmet['opis'])
                return predmet['broj_kredita']
 
        print('Predmet ne postoji.')
        return 0
