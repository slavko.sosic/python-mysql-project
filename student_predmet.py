import os
import sys
sys.path.append(os.getcwd())

from queries.student_predmet import insert_student_predmet


"""Premjsetiti ovu klasu u classes folder kad su sve ostale tamo"""
class StudentPredmet:
    def __init__(self, insert_in_db, student_id, predmet_id, status='polozen'):
        self.student_id = student_id
        self.predmet_id = predmet_id
        self.status = status

        if insert_in_db:
            try:
                insert_student_predmet(student_id, predmet_id, status)
            except:
                print('Ne može se dodati ovaj student-predmet')

    def set_student_id(self, student_id):
        self.student_id = student_id

    def get_student_id(self):
        return self.student_id
    
    def set_predmet_id(self, predmet_id):
        self.predmet_id = predmet_id

    def get_predmet_id(self):
        return self.predmet_id
    
    def set_status(self, status):
        self.status = status

    def get_status(self):
        return self.status
    