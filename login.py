import re
from queries.login_queries import sign_in
from sluzba_upis import sluzba_upis
import time
from upisi_studenta import upisi_studenta
from sluzba_edit import sluzba_edit


def login(korisnik):
    while True:
        korisnik['uloga'] = input('Uloga (ili izlaz):\n')
        if call_exit_from_app(korisnik['uloga']):
            return True

        if (korisnik['uloga'] != 'student' and
                korisnik['uloga'] != 'sluzba'):
            print('Ponovi unos')
            continue

        while korisnik['uloga'] == 'sluzba':
            korisnik['email'] = input('Unesite email (ili izlaz):\n')
            if call_exit_from_app(korisnik['email']):
                return True

            if is_valid_email(korisnik['email']):
                break
            print('Neispravan unos')

        while True:
            korisnik['sifra'] = input('Unesite lozinku (ili izlaz):\n')
            if call_exit_from_app(korisnik['sifra']):
                return True

            if korisnik['sifra'] == '':
                print('Neispravan unos')
                continue
            break
        while korisnik['uloga'] == 'sluzba':
            korisnik['svrha'] = input('Svrha prijavljivanja (edit, upis ili izlaz):\n')
            if call_exit_from_app(korisnik['svrha']):
                return True

            if (korisnik['svrha'] != 'edit' and
                    korisnik['svrha'] != 'upis'):
                print('Neispravan unos')
                continue
            break
        login_info = sign_in(korisnik)
        if login_info is None:
            print('Bad login, try again!')
            continue
        print('Dobro dosli')

        return False


def call_exit_from_app(text):
    if text == 'izlaz':
        print('End!')
        return True
    return False


def is_valid_email(email):
    email_regex = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
    return True if re.match(email_regex, email) else False


# pocetak
korisnik = {}
exit_from_app = login(korisnik)

# --------- student ---------------

while korisnik['uloga'] == 'student' and not exit_from_app:

    upisi_studenta(korisnik['sifra'])
    answer = input('Zelite da nastavite?\n')
    if answer.lower() == 'da':
        continue
    break
# --------- sluzba ---------------

while korisnik['uloga'] == 'sluzba' and not exit_from_app:
    if korisnik['svrha'] == 'upis':
        sluzba_upis(korisnik['email'])
        pass
    else:
        sluzba_edit()
        pass
    answer = input('Zelite da nastavite?\n')
    if answer.lower() == 'da':
        korisnik['svrha'] = input('Zelite upis ili edit?\n')
        continue
    break

time.sleep(2)
print('Kraj.')
