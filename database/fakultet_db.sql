CREATE DATABASE  IF NOT EXISTS `fakultet` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `fakultet`;
-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: fakultet
-- ------------------------------------------------------
-- Server version	8.0.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `predmet`
--

DROP TABLE IF EXISTS `predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `predmet` (
  `predmet_id` int NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) NOT NULL,
  `opis` text NOT NULL,
  `broj_kredita` int NOT NULL,
  `godina_studija` int NOT NULL,
  `profesor_id` int NOT NULL,
  PRIMARY KEY (`predmet_id`),
  UNIQUE KEY `profesor_id_UNIQUE` (`profesor_id`),
  CONSTRAINT `fk_predmet_profesor` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`profesor_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predmet`
--

LOCK TABLES `predmet` WRITE;
/*!40000 ALTER TABLE `predmet` DISABLE KEYS */;
INSERT INTO `predmet` VALUES (1,'napredne tehnologije','startup aplikacije',6,1,1),(3,'Baze I','sql server, c#, praljenje 2d igrice... er dijagram',8,2,2),(5,'uvod java','java programiranje',15,1,3),(6,'analiza I','limesi, izvodi',7,2,4),(7,'modeliranje','rad sa zadacima ',5,2,5),(8,'anatomija','latinski nazivi, nervni sistem',57,2,6);
/*!40000 ALTER TABLE `predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesor`
--

DROP TABLE IF EXISTS `profesor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profesor` (
  `profesor_id` int NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) NOT NULL,
  `prezime` varchar(45) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`profesor_id`),
  UNIQUE KEY `profesor_id_UNIQUE` (`profesor_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesor`
--

LOCK TABLES `profesor` WRITE;
/*!40000 ALTER TABLE `profesor` DISABLE KEYS */;
INSERT INTO `profesor` VALUES (1,'srdjan','kadic','filip.jovanovic@bixbit.me'),(2,'predrag','stanisic','anja.stijepovic@bixbit.me'),(3,'Boro','Markovic','boro.markovic@bixbit.me'),(4,'Aco','Popovic','nikola.lalicic@bixbit.me'),(5,'Milenko','Mosurovic','milenko@ucg.me'),(6,'Nikola','Konatar','konti@ucg.me'),(7,'anja','vukalovic','slavko.sosic@bixbit.me'),(10,'jaksa','vlahovic','jaksa.vlahovic@bixbit.me');
/*!40000 ALTER TABLE `profesor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sluzba`
--

DROP TABLE IF EXISTS `sluzba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sluzba` (
  `sluzba_id` int NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) NOT NULL,
  `sifra` varchar(64) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`sluzba_id`),
  UNIQUE KEY `sluzba_id_UNIQUE` (`sluzba_id`),
  UNIQUE KEY `ime_UNIQUE` (`ime`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sluzba`
--

LOCK TABLES `sluzba` WRITE;
/*!40000 ALTER TABLE `sluzba` DISABLE KEYS */;
INSERT INTO `sluzba` VALUES (1,'pmf','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3','slavko.sosic@bixbit.me'),(2,'etf','ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad','sluzba.etf@gmail.com');
/*!40000 ALTER TABLE `sluzba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `student_id` int NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) NOT NULL,
  `prezime` varchar(45) NOT NULL,
  `JSK` varchar(20) NOT NULL,
  `broj_indexa` varchar(6) NOT NULL,
  `godina_studija` int NOT NULL,
  `status` varchar(45) NOT NULL,
  `balans` decimal(6,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`student_id`),
  UNIQUE KEY `JSK_UNIQUE` (`JSK`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'marko','markovic','123','8/20',1,'Samofinansiranje',0.00),(2,'janko','jankovic','asdf','9/19',2,'budzet',0.00),(3,'ivan','ivanovic','abcd','123/20',3,'samofinansiranje',500.00),(4,'milica','markovic','321','54/22',2,'samofinansiranje',1000.00);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_predmet`
--

DROP TABLE IF EXISTS `student_predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_predmet` (
  `student_id` int NOT NULL,
  `predmet_id` int NOT NULL,
  `status` enum('prijavljen','polozen') NOT NULL DEFAULT 'prijavljen',
  PRIMARY KEY (`student_id`,`predmet_id`),
  KEY `fk_student_predmet_2` (`predmet_id`),
  CONSTRAINT `fk_student_predmet_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_student_predmet_2` FOREIGN KEY (`predmet_id`) REFERENCES `predmet` (`predmet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_predmet`
--

LOCK TABLES `student_predmet` WRITE;
/*!40000 ALTER TABLE `student_predmet` DISABLE KEYS */;
INSERT INTO `student_predmet` VALUES (1,1,'prijavljen'),(1,5,'prijavljen');
/*!40000 ALTER TABLE `student_predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upis`
--

DROP TABLE IF EXISTS `upis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `upis` (
  `upis_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `skolska_godina` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_toku` tinyint NOT NULL,
  PRIMARY KEY (`upis_id`,`student_id`),
  UNIQUE KEY `upis_id_UNIQUE` (`upis_id`),
  UNIQUE KEY `student_id_UNIQUE` (`student_id`),
  CONSTRAINT `fk_upis_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upis`
--

LOCK TABLES `upis` WRITE;
/*!40000 ALTER TABLE `upis` DISABLE KEYS */;
INSERT INTO `upis` VALUES (1,1,'2024-02-05 14:31:37',0),(2,2,'2024-02-05 14:32:00',0),(3,3,'2024-02-05 14:47:08',0),(4,4,'2022-02-05 14:47:08',0);
/*!40000 ALTER TABLE `upis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'fakultet'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-04 10:04:16
