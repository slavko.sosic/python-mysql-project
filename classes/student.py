# Student ima atribute ime, prezime, JSK, broj_indeksa,
# godina_studija, prijavljeni_predmeti, polozeni_predmeti
# (za ovaj dio pogledajte Many-To-Many odnose),
# status, balans (sa default vrijednoscu None).

import os
import sys
sys.path.append(os.getcwd())
"""
Ne treba ti zagrada kod importa
"""
from queries.student import (get_student_from_jsk_query, insert_student, delete_student, 
                            update_ime, update_prezime, update_JSK, update_broj_indeksa,
                            update_godina_studija, update_status, update_balans,
                            update_student_from_jsk_query, delete_student_from_jsk_query)
from queries.student_predmet import update_prijavljeni_predmeti, update_polozeni_predmeti
from queries.predmet import get_id_from_name


class Student:
    def __init__(self, insert_in_db, ime, prezime, JSK, broj_indeksa, godina_studija,
                 prijavljeni_predmeti, polozeni_predmeti, status, balans=None):
        self.ime = ime
        self.prezime = prezime
        self.JSK = JSK
        self.broj_indeksa = broj_indeksa
        self.godina_studija = godina_studija
        self.prijavljeni_predmeti = prijavljeni_predmeti
        self.polozeni_predmeti = polozeni_predmeti
        self.status = status
        self.balans = balans

        if insert_in_db:
            insert_student(self)

    def set_ime(self, ime):
        self.ime = ime
        update_ime(self, ime)

    def get_ime(self):
        return self.ime

    def set_ime(self, prezime):
        self.prezime = prezime
        update_prezime(self, prezime)

    def get_prezime(self):
        return self.prezime

    def set_JSK(self, JSK):
        self.JSK = JSK
        update_JSK(self, JSK)

    def get_JSK(self):
        return self.JSK

    def set_broj_indeksa(self, broj_indeksa):
        self.broj_indeksa = broj_indeksa
        update_broj_indeksa(self, broj_indeksa)

    def get_broj_indeksa(self):
        return self.broj_indeksa

    def set_godina_studija(self, godina_studija):
        self.godina_studija = godina_studija
        update_godina_studija(self, godina_studija)

    def get_godina_studija(self):
        return self.godina_studija

    def set_prijavljeni_predmeti(self, prijavljeni_predmeti):
        self.prijavljeni_predmeti = prijavljeni_predmeti
        prijavljeni_predmeti_id = [get_id_from_name(predmet['ime']) for predmet in prijavljeni_predmeti]
        update_prijavljeni_predmeti(self, prijavljeni_predmeti_id)

    def get_prijavljeni_predmeti(self):
        return self.prijavljeni_predmeti

    def set_polozeni_predmeti(self, polozeni_predmeti):
        self.polozeni_predmeti = polozeni_predmeti
        polozeni_predmeti_id = [get_id_from_name(predmet['ime']) for predmet in polozeni_predmeti]
        update_polozeni_predmeti(self, polozeni_predmeti_id)

    def get_polozeni_predmeti(self):
        return self.polozeni_predmeti

    def set_status(self, status):
        self.status = status
        update_status(self, status)

    def get_status(self):
        return self.status

    def set_balans(self, balans):
        self.balans = balans
        update_balans(self, balans)

    def get_balans(self):
        return self.balans
    
    def delete_student(self):
        delete_student(self)
    
    @staticmethod
    def update_student_from_jsk(jsk, change, new_value):
        update_student_from_jsk_query(jsk, change, new_value)

    @staticmethod
    def get_student_from_jsk(jsk):
        return get_student_from_jsk_query(jsk)

    @staticmethod
    def delete_student_from_jsk(jsk):
        delete_student_from_jsk_query(jsk)

# student1 = Student(False, 'filip', 'jovanovic', '1234lj', '1/23', 3, [1], [1], 'budzet')
# print(Student.get_student_from_jsk('1234lj'))
#Student.delete_student_from_jsk('1234lj')   
        
'''
- get student from jsk 
- delete st from jsk 
- 1 update uz change
'''