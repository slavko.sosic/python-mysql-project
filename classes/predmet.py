import csv
import os
import sys
sys.path.append(os.getcwd())
from queries.predmet_queries import insert_predmet, update_predmet, delete_predmet, get_predmet_from_name



class Predmet: 
    def __init__(self,ime, opis, broj_kredita, godina_studija, profesor_id, novi_unos = True ):  # Kod default vrijednosti parametara ne stavljamo ramake izmejdui jednakosti (npr. novi_unos=True)
        self.__ime = ime
        self.__opis = opis
        self.__broj_kredita  = broj_kredita  # 2 space prije jednako stoje
        self.__godina_studija = godina_studija
        self.__profesor_id = profesor_id
       
        if novi_unos:
            insert_predmet(ime, opis, broj_kredita, godina_studija, profesor_id)
            #print(f'Predmet {ime} je dodat u bazu')
        
    
    @property
    def get_ime(self):
        return self.__ime
   
    @property
    def get_opis(self):
        return self.__opis

    @property
    def get_broj_kredita(self):
        return self.__broj_kredita
    
    @property
    def get_godina_studija(self):
        return self.__godina_studija
    
    @property
    def get_profesor_id(self):
        return self.__profesor_id

    # update_predmet_value je staticka, ne koristi se self
    def update_predmet_value(self,ime, change, value):
       return update_predmet(ime,change, value)

    # Ovu funkcije koristite za prikaz u svim ostalim funkcijama, ako je vec tako,
    # trebalo je malo uobliciti output, da ne bude samo tuple sa sve id-em iz baze
    # umjesto profesor id trebalo je ime profesora, korisniku nista znaci broj iz baze
    @staticmethod
    def get_predmet(ime):   # koristi tab za indentaciju
       return get_predmet_from_name(ime)    
    
    
    @staticmethod
    def predmet_delete(ime):
        return delete_predmet(ime)



