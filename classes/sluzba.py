'''
Napraviti klasu Služba sa atributima ime, šifra (pogledajte čuvanje šifre – možete koristiti sha256 algoritam, postoji python biblioteka), email.
'''
import os
import sys
sys.path.append(os.getcwd())

import hashlib
import re

from queries.queries_sluzba import add_sluzba, update_name, update_password, update_email, delete_sluzba, get_all_from_sluzba


class Sluzba:
    def __init__(self, ime, sifra, email, novi_unos = True):
        
        self.ime = ime
        self.email = self.validate_email(email)
        self.sifra = self.hash_password(self.validate_password(sifra))

        #Novi unos
        if novi_unos:
            add_sluzba(self.ime, self.sifra, self.email)
            print(f'Sluzba {ime} je dodata u bazu')

        #add sluzba instance only when it doesnt already exist

    @staticmethod
    def hash_password(password):
        password_bytes = password.encode('utf-8')
        hash_object = hashlib.sha256(password_bytes)
        password_hash = hash_object.hexdigest()
        return password_hash
    
    @staticmethod
    def validate_password(password):
        pattern = r"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}$"
        match = re.match(pattern, password)
        if match:
            return password
        raise ValueError("Invalid password")
    #Password must be at least 7 characters long, containing at least one uppercase and lowercase letters and at least one digit

    @staticmethod
    def validate_email(email):
        pattern=r"\b[\w]+@[\w.-]+\.\w+$"
        match = re.match(pattern, email)
        if match:
            return email
        raise ValueError("Invalid email")
    
 
    def set_ime(self, ime):
        self.ime = update_name(self.email, ime)
        

    def set_sifra(self, sifra):
        self.sifra = update_password(self.email, sifra)

    def set_email(self, email):
        self.email = update_email(self.email, email)
    
    @staticmethod
    def delete_from_table(email):
        delete_sluzba(email)
    """
    Ovu funkcijU koristite za prikaz u svim ostalim funkcijama, ako je vec tako, trebalo je malo uobliciti output, da ne bude samo tuple sa sve id iz baze
    """
    @staticmethod
    def get_sluzba():
        return get_all_from_sluzba()


# sluzba_1 = Sluzba('pmf', 'ABCabc9', 'pmf@gmail.com', False)


#sluzba_1.set_ime('pmf')
#sluzba_1.set_sifra('45ahhaHA')
#sluzba_1.set_email('finansije3@gmail.com')

# Sluzba.delete_from_table('novasluzba@gmail.com')

# print(Sluzba.get_sluzba())