import csv
import os
import sys
import re
sys.path.append(os.getcwd())
from queries.profesor_queries import insert_profesor, update_profesor, delete_profesor, get_from_profesor_email


"""
Klasa Profesor:
   Paziti na formatiranje koda:
    - koristiti Tab (ili 4 space) za uvlačenje koda (npr. kod funkicje update_profesor_value)
    - Kod default vrijednosti parametara ne stavljamo ramake izmejdui jednakosti (npr. novi_unos=True)
    - Kod imenovanja klasa i funkcija ne trebe da postoje razmak prije : (npr. class Profesor:)
    - Posle svakog argumneta funkcije treba da postoji razmak (npr. def __init__(self, ime, prezime, email, novi_unos=True):)
    
"""

class Profesor :
    def __init__(self, ime, prezime, email, novi_unos=True):
        self._ime = ime
        self._prezime = prezime
        self.email=  email  # pazi formatiranje

        # Novi unos
        if novi_unos:
            insert_profesor(ime, prezime, email)
            #print(f'Profesor {ime} {prezime} je dodat u bazu')

    @property    
    def get_ime(self):
        return self.ime
    
    @property    
    def get_prezime(self):
        return self.prezime
    
    @property    
    def get_email(self):
        return self.email

    # Funkcija update_profesor_value: je staticka, ne koristi se self

    def update_profesor_value(self,email, change, value):
       return update_profesor(email,change, value)    


    @staticmethod
    def profesor_delete(email):
       return delete_profesor(email)


    # Ovu funkcije koristite za prikaz u svim ostalim funkcijama, ako je vec tako, trebalo je malo uobliciti outpu, da ne bude samo tuple sa sve id iz baze
    @staticmethod
    def get_profesor(email):
       return get_from_profesor_email(email)





   
# profesor_1 = Profesor("Darko","Darkovic", "darko.darkovic@gamil.com", False)


# profesor_1.update_profesor_value('darko.darkovic@gamil.com', 'ime', 'Ranko')




        