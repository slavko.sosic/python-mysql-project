'''
1. U fajlu classes.py pišete sve klase koje će kasnije biti definisane. Obratite pažnju da u setteru morate pozvati UPDATE iz baze,
 prilikom pravljenja nove instance u konstruktoru INSERT i konačno, napraviti i metodu delete, povezan sa DELETE upitom. 
 Upite pišite u poseban fajl/ove i iz njega samo pozivajte te funkcije unutar vaših metoda (queries\test.py). 
 Za šta god možete napravite validaciju (obavezno email i šifre – postaviti uslov za šifru npr. veliko, malo slovo, broj,  dužina makar 7 – koristite regex!). 
 Raisujute grešku sa porukom za nevalidan unos. Nakon isprintane poruke ponovo se zahtijeva unos.

2. Napravite klasu Upis koja ima atribute student, školska_godina (datetime tipa) i u_toku (boolean tipa). 
Napravite statičku metodu koja ažurira školsku godinu u skladu sa trenutnim datumom.
'''
import os
import sys
sys.path.append(os.getcwd())

import hashlib
import re
from datetime import datetime

from queries.queries_upis import add_upis, update_skolska_godina, update_u_toku, delete_upis, get_all_from_upis

class Upis:
    def __init__(self, student_id, skolska_godina, u_toku, novi_unos = True):  # Kod default vrijednosti parametara ne stavljamo razake izmedj jednakosti (npr. novi_unos=True)
        
        self.student_id = student_id

        if not isinstance(skolska_godina, datetime):
            raise ValueError("Školska godina mora biti datetime tipa")
        self.skolska_godina = skolska_godina

        if u_toku not in {0,1}:
            raise ValueError("U_toku mora biti 1 ili 0")
        self.u_toku = u_toku

        if novi_unos:
            add_upis(self.student_id, self.skolska_godina, self.u_toku)
            print(f'Upis studenta {student_id} je dodat u bazu')

    

    def set_skolska_godina(self, skolska_godina):
        self.skolska_godina = update_skolska_godina(self.student_id, skolska_godina)

    def set_u_toku(self, u_toku):
        self.u_toku = update_u_toku(self.student_id, u_toku)

    
    """
    Metode moraju imate self kao prvi parametar
    """

    @staticmethod
    def delete_from_table(student_id):
        delete_upis(student_id)
    
    @staticmethod
    def get_upis():
        return get_all_from_upis()
    
# upis_3 = Upis(3, datetime(2024, 2, 5), 0, False)

# print(upis_3.__dict__)


#upis_3.set_skolska_godina(datetime(2023, 2, 5))

#upis_3.update_school_year()

# upis_3.set_u_toku(1)

#upis_3.set_skolska_godina(datetime.now())

# print(Upis.get_upis())

# Upis.delete_from_table(1)


