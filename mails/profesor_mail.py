import os
import sys
sys.path.append(os.getcwd())
import base64
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from requests import HTTPError
def send_mail(send_to_mail, email):
    SCOPES = [
        "https://www.googleapis.com/auth/gmail.send"
    ]
    flow = InstalledAppFlow.from_client_secrets_file("database/config.json", SCOPES)

    creds = flow.run_local_server(port=0)
    service = build('gmail', 'v1', credentials=creds)

    message = MIMEMultipart()
    message['to'] = send_to_mail
    message['subject'] = f'Izvještaj za upisnu {email} godinu'

    html_content = """
    <html>
    <head></head>
    <body>
    <h1>Izvještaj za studensku službu fakulteta</h1>
    <hr/>
    <p><b>Poštovani</b>,</p>
    <p>U prilogu je poslat <i>.csv</i> fajl evidencije za tražene predmete.</p>
    <p>Srdačan pozdrav,</p>
    <p></p>
    </body>
    </html>
    """
    html_part = MIMEText(html_content, 'html')
    message.attach(html_part)
    file_name = f'{email}.csv'
    with open(f'output/{file_name}', 'rb') as file:
        csv_part = MIMEApplication(file.read(), Name=file_name)
    csv_part['Content-Disposition'] = f'attachment; filename="{file_name}"'
    message.attach(csv_part)

    raw_message = base64.urlsafe_b64encode(message.as_bytes()).decode()
    create_message = {'raw': raw_message}
    try:
        message = (service.users().messages().send(userId="me", body=create_message).execute())
        print(f"Pismo uspjesno poslato na adresu: {send_to_mail}")
    except HTTPError as error:
        print(f'An error occurred: {error}')
        message = None

#send_mail('nikolalalicic5@gmail.com', 'baze podataka')
