'''
Napravite funkciju služba_edit. Nakon logina, printa se da li želi da promijeni informacije o Profesorima, 
Studentima ili Predmetima. Zatim da li želi da odradi get, delete, update ili insert u neku od tabela.
Zahtjev se zatim procesuje, validira i ažurira se baza u skladu s njim. 
Npr. ako je student na samofinansiranju i uplati novac,
balans se povećava za tu cijenu i treba uraditi update te tabele. Ukoliko je na budžetu, printati grešku.

'''

import os
import sys
sys.path.append(os.getcwd())

import hashlib
import re
import time

from classes.profesor import Profesor

from classes.predmet import Predmet

from classes.student import Student

def is_valid_email(email):
    email_regex = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
    return True if re.match(email_regex, email) else False


# PROFESOR, STUDENT ILI PREDMET

# def prompt_user_for_table_name():
#     while True:
#         ime_tabele = input("Unesite ime tabele (profesor, student ili predmet) u kojoj želite izmijeniti podatke:\n")
#         if ime_tabele.lower() not in ['profesor', 'student', 'predmet']:
#             print("Nepostojeća tabela!")
#             continue
#         return ime_tabele

def prompt_user_for_table_name():
    while True:
        print("\nTabele:")
        print("1. profesor")
        print("2. student")
        print("3. predmet")
        
        korisnicki_unos = input("Unesite broj ili naziv tabele:\n >>")
        
        if korisnicki_unos.lower() in ['profesor', 'student', 'predmet']:
            return korisnicki_unos.lower()
        elif korisnicki_unos == '1':
            return 'profesor'
        elif korisnicki_unos == '2':
            return 'student'
        elif korisnicki_unos == '3':
            return 'predmet'
        else:
            print("Nepostojeća opcija. Ponovite unos.")




# GET DELETE UPDATE ILI INSERT
# def prompt_user_for_action():
#     akcija = input("Želite li da uradite get, delete, update ili insert u izabranoj tabeli?\n")
#     if akcija.lower() not in ['get', 'delete', 'update', 'insert']:
#         print("Nepostojeća akcija!")
#     return akcija

def prompt_user_for_action():
    while True:
        print("\nAkcije:")
        print("1. get")
        print("2. delete")
        print("3. update")
        print("4. insert")
        
        korisnicki_unos = input("Unesite broj ili naziv akcije koju želite izvršiti nad izabranom tabelom:\n >>")
        
        if korisnicki_unos.lower() in ['get', 'delete', 'update', 'insert']:
            return korisnicki_unos.lower()
        elif korisnicki_unos == '1':
            return 'get'
        elif korisnicki_unos == '2':
            return 'delete'
        elif korisnicki_unos == '3':
            return 'update'
        elif korisnicki_unos == '4':
            return 'insert'
        else:
            print("Nepostojeća opcija. Ponovite unos.\n")


# EMAIL PROFESORA
def prompt_user_for_professor_email():
    while True:
        input_email_profesora = input("Unesite email profesora:\n >>")
        profesor = Profesor.get_profesor(input_email_profesora)
        if profesor is not None: # Dovoljno if profesor: jer je None "falsy"
            return input_email_profesora
        else:
            print(f"Profesor sa emailom {input_email_profesora} ne postoji u bazi. Ponovite unos.\n")
            

#STUDENT JSK 
def prompt_user_for_student_jsk():
    while True:
        input_jsk_studenta = input("Unesite JSK studenta:\n >>")
        student = Student.get_student_from_jsk(input_jsk_studenta)
        if student is not None: # isto kao iz za profesora
            return input_jsk_studenta
        print(f"Student {input_jsk_studenta} ne postoji u bazi. Ponovite unos.\n")



# IME PREDMETA
def prompt_user_for_subject_name():
    while True:
        input_ime_predmeta =  input("Unesite ime predmeta:\n >>")
        predmet = Predmet.get_predmet(input_ime_predmeta)
        if predmet is not None:
            return input_ime_predmeta
        print(f"Predmet {input_ime_predmeta} ne postoji u bazi. Ponovite unos.\n")


# GET AKCIJA
def get_akcija(tabela):
        if tabela == 'profesor':
            input_email_profesora = prompt_user_for_professor_email()
            print(Profesor.get_profesor(input_email_profesora))
            
        if tabela == 'student':
            input_jsk_studenta = prompt_user_for_student_jsk()
            print(Student.get_student_from_jsk(input_jsk_studenta))
            
        if tabela == 'predmet':
            input_ime_predmeta = prompt_user_for_subject_name()
            print(Predmet.get_predmet(input_ime_predmeta))


# DELETE AKCIJA       
def delete_akcija(tabela):
    if tabela == 'profesor':
       while True:
            input_email_profesora = input("Unesite email profesora:\n >>")
            profesor = Profesor.get_profesor(input_email_profesora)
            if profesor is None:
                print(f"Profesor sa emailom {input_email_profesora} ne postoji u bazi. Ponovite unos.\n")
            else:  
                Profesor.profesor_delete(input_email_profesora)
                print(f"Profesor sa emailom {input_email_profesora} je obrisan iz baze.")
                break
        
    if tabela == 'student':
        while True:
            input_jsk_studenta = input("Unesite JSK studenta:\n >>")
            student = Student.get_student_from_jsk(input_jsk_studenta)
            if student is None:
                print(f"Student {input_jsk_studenta} ne postoji u bazi. Ponovite unos.\n")
            else:
                Student.delete_student_from_jsk(input_jsk_studenta)
                print(f"Student {input_jsk_studenta} je obrisan iz baze.")
                break
        
    if tabela == 'predmet':
        while True:
            input_ime_predmeta = input("Unesite ime predmeta:\n >>")
            predmet = Predmet.get_predmet(input_ime_predmeta)
            if predmet is None:
                print(f"Predmet {input_ime_predmeta} ne postoji u bazi. Ponovite unos.\n")
            else:
                Predmet.predmet_delete(input_ime_predmeta)
                print(f"Predmet {input_ime_predmeta} je obrisan iz baze.")
                break    

# KOLONE PROFESORA

def prompt_user_for_professor_column():
        while True:
            print("\nKolone u tabeli profesor:")
            print("1. ime")
            print("2. prezime")
            print("3. email")
            input_ime_kolone = input("Unesite broj ili naziv kolone u tabeli profesor u kojoj želite da izmijenite neku vrijednost:\n >>")
            if input_ime_kolone.lower() in ['ime', 'prezime', 'email']:
                return input_ime_kolone.lower()
            elif input_ime_kolone == '1':
                return 'ime'
            elif input_ime_kolone == '2':
                return 'prezime'
            elif input_ime_kolone == '3':
                return 'email'
            else:
                print("Nepostojeća opcija. Ponovite unos.\n")


# KOLONE STUDENTA
                
def prompt_user_for_student_column():
    while True:
        print("\nKolone u tabeli student:")
        print("1. ime")
        print("2. prezime")
        print("3. jsk")
        print("4. broj_indeksa")
        print("5. godina_studija")
        print("6. status")
        print("7. balans")
        
        input_ime_kolone = input("Unesite broj ili naziv kolone u tabeli student u kojoj želite da izmijenite neku vrijednost:\n >>")
        
        if input_ime_kolone.lower() in ['ime', 'prezime', 'jsk', 'broj_indeksa', 'godina_studija', 'status', 'balans']:
            return input_ime_kolone.lower()
        elif input_ime_kolone == '1':
            return 'ime'
        elif input_ime_kolone == '2':
            return 'prezime'
        elif input_ime_kolone == '3':
            return 'jsk'
        elif input_ime_kolone == '4':
            return 'broj_indeksa'
        elif input_ime_kolone == '5':
            return 'godina_studija'
        elif input_ime_kolone == '6':
            return 'status'
        elif input_ime_kolone == '7':
            return 'balans'
        else:
            print("Nepostojeća opcija. Ponovite unos.\n")

#KOLONE PREDMETA
            
def prompt_user_for_subject_column():
    while True:
        print("\nKolone u tabeli predmet:")
        print("1. ime")
        print("2. opis")
        print("3. broj_kredita")
        print("4. godina_studija")
        print("5. profesor_id")
        
        input_ime_kolone = input("Unesite broj ili naziv kolone u tabeli predmet u kojoj želite da izmijenite neku vrijednost:\n >>")
        
        if input_ime_kolone.lower() in ['ime', 'opis', 'broj_kredita', 'godina_studija', 'profesor_id']:
            return input_ime_kolone.lower()
        elif input_ime_kolone == '1':
            return 'ime'
        elif input_ime_kolone == '2':
            return 'opis'
        elif input_ime_kolone == '3':
            return 'broj_kredita'
        elif input_ime_kolone == '4':
            return 'godina_studija'
        elif input_ime_kolone == '5':
            return 'profesor_id'
        else:
            print("Nepostojeća opcija. Ponovite unos.\n")


# UPDATE AKCIJA      
        
# profesor update

def update_akcija(tabela):

    if tabela == 'profesor':
        input_email_profesora = prompt_user_for_professor_email()
        input_ime_kolone_profesora = prompt_user_for_professor_column()
        nova_vrijednost_profesora = input(f"Unesite novu vrijednost za izabranog profesora i kolonu {input_ime_kolone_profesora}:\n >>")
        profesor_id, ime, prezime, mail =  Profesor.get_profesor(input_email_profesora)
        profesor_novi = Profesor(ime, prezime, mail, False)
        profesor_novi.update_profesor_value(input_email_profesora, input_ime_kolone_profesora, nova_vrijednost_profesora)
        print(f"Podatak o profesoru sa emailom {input_email_profesora} je ažuriran.") 
        
# student update
    if tabela == 'student':
        input_jsk_studenta = prompt_user_for_student_jsk()
        input_ime_kolone_studenta = prompt_user_for_student_column()

        #sad odje treba logika za balans i kad se sta updejtuje
        nova_vrijednost_studenta = input(f"Unesite novu vrijednost za studenta {input_jsk_studenta} i kolonu {input_ime_kolone_studenta}:\n >>")
        Student.update_student_from_jsk(input_jsk_studenta, input_ime_kolone_studenta, nova_vrijednost_studenta)
        print(f"Podatak o studentu {input_jsk_studenta} je ažuriran.") 
       
# predmet update
    if tabela == 'predmet':
        input_ime_predmeta = prompt_user_for_subject_name()
        input_ime_kolone_predmeta = prompt_user_for_subject_column()
        nova_vrijednost_predmeta = input(f"Unesite novu vrijednost za izabrani predmet i kolonu {input_ime_kolone_predmeta}:\n")
        predmet_id, ime, opis, broj_kredita, godina_studija, profesor_id =  Predmet.get_predmet(input_ime_predmeta)
        predmet_novi = Predmet(ime, opis, broj_kredita, godina_studija, profesor_id, False)
        predmet_novi.update_predmet_value(input_ime_predmeta, input_ime_kolone_predmeta, nova_vrijednost_predmeta)
        print(f"Podatak o predmetu {input_ime_predmeta} je ažuriran.")   
         
# INSERT AKCIJA
        
# profesor insert
        
def insert_akcija(tabela):
    if tabela == 'profesor':
        while True:
            novi_profesor_ime = input("Unesite ime za novog profesora:\n")
            novi_profesor_prezime = input("Unesite prezime za novog profesora:\n")
            novi_profesor_email = input("Unesite email za novog profesora:\n")
        
            novi_profesor = Profesor.get_profesor(novi_profesor_email)
            if novi_profesor is not None: 
                print(f"Profesor sa emailom {novi_profesor_email} već postoji u bazi.")
            elif not is_valid_email(novi_profesor_email):
                print(f"Neispravna email adresa.")
                continue
            else:
                Profesor(novi_profesor_ime, novi_profesor_prezime, novi_profesor_email, True)
                print(f"Profesor sa emailom {novi_profesor_email} je dodat u bazu.")
                break

    
     #while true           
#if is_valid_email(novi_profesor_email): 
         
        
#student insert
            
    if tabela == 'student':
        ime = input("Unesite ime novog studenta:\n")
        prezime = input("Unesite prezime novog studenta:\n")
        jsk = input("Unesite jsk novog studenta:\n")
        broj_indeksa = input("Unesite broj indeksa novog studenta:\n")
        godina_studija = input("Unesite godinu studija novog studenta:\n")
        status = input("Unesite status (budzet ili samofinansiranje) novog studenta:\n")
        balans = input("Unesite balans novog studenta:\n")

        novi_student = Student.get_student_from_jsk(jsk)
        if novi_student is not None:
            print(f"Student {jsk} već postoji u bazi")
        else:
            Student(True, ime, prezime, jsk, broj_indeksa, godina_studija, [1], [1], status, balans)
            print(f"Student {jsk} je dodat u bazu")
            
    
#predmet insert
            
    if tabela == 'predmet':
        novi_predmet_ime = input("Unesite ime za novi predmet:\n")
        novi_predmet_opis = input("Unesite opis za novi predmet:\n")
        novi_predmet_broj_kredita = input("Unesite broj kredita za novi predmet:\n")
        novi_predmet_godina_studija = input("Unesite godinu studija za novi predmet:\n")
        novi_predmet_profesor_id = input("Unesite profesor id za novi predmet:\n")

        novi_predmet = Predmet.get_predmet(novi_predmet_ime)
        if novi_predmet is not None:
            print(f"Predmet {novi_predmet_ime} već postoji u bazi.")
        else:
            Predmet(novi_predmet_ime, novi_predmet_opis, novi_predmet_broj_kredita, novi_predmet_godina_studija, novi_predmet_profesor_id, True)
            print(f"Predmet {novi_predmet_ime} je dodat u bazu.")
            


# SLUZBA EDIT
    
def sluzba_edit():
    while True:
        tabela = prompt_user_for_table_name()
        akcija = prompt_user_for_action()
        if akcija == 'get':
            get_akcija(tabela)
        elif akcija == 'delete':
            delete_akcija(tabela)
        elif akcija == 'update':
            update_akcija(tabela)
        elif akcija == 'insert':
            insert_akcija(tabela)

        odgovor = input("\nDa li želite obaviti još neku akciju nad tabelama? (da/ne): ")
        if odgovor.lower() != 'da':
            break 

# sluzba_edit()


'''
        while True:
            input_email_profesora = input("Unesite email profesora:\n >>")
            profesor = Profesor.get_profesor(input_email_profesora)
            if profesor is not None:
                return input_email_profesora
            else:
                print(f"Profesor sa emailom {input_email_profesora} ne postoji u bazi. Ponovite unos.\n")
            break
'''

# time.sleep(2)