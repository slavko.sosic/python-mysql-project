import os
import sys
sys.path.append(os.getcwd())
from database.db import db, cursor

def insert_predmet(ime, opis, broj_kredita, godina_studija, profesor_id):
    sql = ("INSERT INTO predmet(ime, opis, broj_kredita, godina_studija, profesor_id) VALUES(%s, %s, %s, %s, %s)")
    cursor.execute(sql, (ime, opis, broj_kredita,godina_studija, profesor_id ))
    db.commit()
    #print(f'Dodaje se predmet {ime}')

#insert_predmet('Programiranje 1', 'osnovi jave', 7, 1, 8)    



def update_predmet(ime, change, value):
        sql_update = (f"UPDATE predmet SET {change} = '{value}' WHERE ime = '{ime}'")
        cursor.execute(sql_update)
        db.commit()



#update_predmet('Programiranje 1','opis','osnove jave')

def delete_predmet(ime):
    sql_delete = (f"DELETE FROM predmet WHERE ime = '{ime}'")
    cursor.execute(sql_delete)
    db.commit()
    #print(f'Predmet {ime} je obrisan.')


def get_predmet_from_name(ime):
    sql_get_all = (f"SELECT * FROM predmet WHERE ime = '{ime}'")
    cursor.execute(sql_get_all)
    row = cursor.fetchone()
    return row


#delete_predmet("Programiranje 1")