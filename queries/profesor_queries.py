import os
import sys
sys.path.append(os.getcwd())
from database.db import db, cursor

def insert_profesor(ime, prezime, email):
    sql = ("INSERT INTO profesor(ime, prezime, email) VALUES(%s, %s, %s)")
    cursor.execute(sql, (ime, prezime, email, ))
    db.commit()



def update_profesor(email, change, value):
        sql_update = (f"UPDATE profesor SET {change} = '{value}' WHERE email = '{email}'")
        cursor.execute(sql_update)
        db.commit()

#update_profesor('test','ime','Boro')

def delete_profesor(email):
    sql_delete = (f"DELETE FROM profesor WHERE email= '{email}'")
    cursor.execute(sql_delete)
    db.commit()
    #print(f'Profesor sa emailom {email} je obrisan.')

# funkcija get_from_profesor_email: ne treba vratati id, jer se ne koristi nigdje.
# Ovdje cak i nije najgora stvar vratiti ga u nekom opstem slucaju, ali ili ovdje ili u klasi si trebao da ga izbacis
def get_from_profesor_email(email):
     sql_get_all = (f"SELECT * FROM profesor WHERE email = '{email}'")
     cursor.execute(sql_get_all)
     row = cursor.fetchone()
     return row


#get_from_profesor_email('srdjan.kadic@gmail.com')
#delete_profesor(16)