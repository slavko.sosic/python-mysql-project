import os
import sys
sys.path.append(os.getcwd())

from database.db import cursor, db
import hashlib
import re


def hash_password(password):
    password_bytes = password.encode('utf-8')
    hash_object = hashlib.sha256(password_bytes)
    password_hash = hash_object.hexdigest()
    return password_hash
    
def validate_password(password):
    pattern = r"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}$"
    match = re.match(pattern, password)
    if bool(match) == True:
        return password
    raise ValueError("Invalid password")

#insert

def add_sluzba(ime, sifra, email):
    sql = ("INSERT INTO sluzba(ime, sifra, email) VALUES (%s, %s, %s)")
    cursor.execute(sql, (ime, sifra, email,))
    db.commit()
    sluzba_id = cursor.lastrowid

#update

def update_name(email, novo_ime):
    sql = ("UPDATE sluzba SET ime = %s WHERE email = %s")
    cursor.execute(sql, (novo_ime, email))
    db.commit()

def update_password(email, nova_sifra):
    sql = ("UPDATE sluzba SET sifra = %s WHERE email= %s")
    nova_sifra = hash_password(validate_password(nova_sifra))
    cursor.execute(sql, (nova_sifra, email))
    db.commit()
    

def update_email(email, novi_email):
    sql = ("UPDATE sluzba SET email = %s WHERE email = %s")
    cursor.execute(sql, (novi_email, email))
    db.commit()

#delete
    
def delete_sluzba(email):
    sql = ("DELETE FROM sluzba WHERE email = %s")
    cursor.execute(sql, (email,))
    db.commit()

"""
NE KORISTI PRINT KAO OUTPUT FUNCKIJE, KORISTI RETURN !!! Van prikaza rezultata, ne moze se koristio ni za sta drugo
Sluzba id se ne korist nigdje, molgli smo i bez njega u selectu.
"""
    
def get_all_from_sluzba():
    sql = ("SELECT ime, sifra, email FROM sluzba")
    cursor.execute(sql)
    result = cursor.fetchall()
    for row in result:
        return row

