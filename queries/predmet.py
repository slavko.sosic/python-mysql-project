# import os
# import sys
# sys.path.append(os.getcwd())

from database.db import db, cursor


def tuple_to_dict(predmet_tuple):
    return {
        "ime": predmet_tuple[0],
        "opis": predmet_tuple[1],
        "broj_kredita": predmet_tuple[2],
        "godina_studija": predmet_tuple[3],
        "profesor_id": predmet_tuple[4]
    }

def get_predmeti_za_godinu_studija(godina_studija):
    cursor.execute("SELECT ime, opis, broj_kredita, godina_studija, profesor_id FROM predmet WHERE godina_studija=%s", (godina_studija, ))
    predmeti_tuples = cursor.fetchall()
    cursor.reset()
    return [tuple_to_dict(predmet) for predmet in predmeti_tuples]

def get_predmet_from_id(predmet_id):
    cursor.execute("SELECT ime, opis, broj_kredita, godina_studija, profesor_id FROM predmet WHERE predmet_id=%s", (predmet_id, ))
    tuple_result = cursor.fetchone()
    cursor.reset()
    return tuple_to_dict(tuple_result)

def get_id_from_name(ime_predmeta):
    cursor.execute("SELECT predmet_id FROM predmet WHERE ime=%s", (ime_predmeta, ))
    result = cursor.fetchone()[0]
    cursor.reset()
    return result
