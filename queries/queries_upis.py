import os
import sys
sys.path.append(os.getcwd())

from database.db import cursor, db
from datetime import datetime

#insert

def add_upis(student_id, skolska_godina, u_toku):
    sql = ("INSERT INTO upis(student_id, skolska_godina, u_toku) VALUES (%s, %s, %s)")
    cursor.execute(sql, (student_id, skolska_godina, u_toku, ))
    db.commit()
    upis_id = cursor.lastrowid

#update

def update_skolska_godina(student_id, nova_skolska_godina):
    sql = ("UPDATE upis SET skolska_godina = %s WHERE student_id = %s")
    cursor.execute(sql, (nova_skolska_godina, student_id))
    db.commit()

def update_u_toku(student_id, u_toku):
    sql = ("UPDATE upis SET u_toku = %s WHERE student_id = %s")
    cursor.execute(sql, (u_toku, student_id))
    db.commit()

#delete
    
def delete_upis(student_id):
    sql = ("DELETE FROM upis WHERE student_id = %s")
    cursor.execute(sql, (student_id,))
    db.commit()

#get
    
def get_all_from_upis():
    sql = ("SELECT skolska_godina, u_toku FROM upis")
    cursor.execute(sql)
    result = cursor.fetchall()
    for row in result:
        return row