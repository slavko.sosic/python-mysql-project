import os
import sys
sys.path.append(os.getcwd())

from database.db import db, cursor

"""
    Izmedju funkcija treba da postoje 2 prazna reda (PEP8)
    Mogao je samo jedan update da ide
    Duplicirane funkcije za brisanje, samo je mal drugaciji argumnet dat
"""
def get_student_from_jsk_query(student_jsk):
    cursor.execute("SELECT * FROM student WHERE JSK=%s", (student_jsk, ))
    result = cursor.fetchone()
    cursor.reset()
    return result


def get_student_id(student):
    cursor.execute("SELECT student_id FROM student WHERE JSK = %s", (student.JSK, ))
    result = cursor.fetchone()[0]
    cursor.reset()
    return result



def get_godina_studija(student):
    cursor.execute("SELECT godina_studija FROM student WHERE JSK = %s", (student.JSK, ))
    result = cursor.fetchone()[0]
    cursor.reset()
    return result

def insert_student(student):
    if student.balans:
        cursor.execute(("INSERT INTO student(ime, prezime, JSK, broj_indexa, godina_studija, status, balans)"
                        "VALUES (%s, %s, %s, %s, %s, %s, %s)"),
                        (student.ime, student.prezime, student.JSK, student.broj_indeksa, student.godina_studija, student.status, student.balans))
    else:
        cursor.execute(("INSERT INTO student(ime, prezime, JSK, broj_indexa, godina_studija, status)"
                        "VALUES (%s, %s, %s, %s, %s, %s)"),
                        (student.ime, student.prezime, student.JSK, student.broj_indeksa, student.godina_studija, student.status))
    db.commit()
    #print(f"Dodat student {student.ime} {student.prezime}.")

def delete_student(student):
    cursor.execute('DELETE FROM student WHERE JSK=%s', (student.JSK, ))
    db.commit()
    print(f"Izbrisan student {student.ime} {student.prezime}.")

def delete_student_from_jsk_query(jsk):
    cursor.execute('DELETE FROM student WHERE JSK=%s', (jsk, ))
    db.commit()
    #print(f"Izbrisan student {jsk}.")

def update_ime(student, ime):
    cursor.execute("UPDATE student SET ime=%s WHERE JSK=%s", (ime, student.JSK))
    db.commit()
    # print(f"Promijenjeno ime studenta {student.ime} {student.prezime}.")

def update_prezime(student, prezime):
    cursor.execute("UPDATE student SET prezime=%s WHERE JSK=%s", (prezime, student.JSK))
    db.commit()
    # print(f"Promijenjeno prezime studenta {student.ime} {student.prezime}.")
    
def update_JSK(student, JSK):
    cursor.execute("UPDATE student SET JSK=%s WHERE JSK=%s", (JSK, student.JSK))
    db.commit()
    # print(f"Promijenjen JSK studenta {student.ime} {student.prezime}.")

def update_broj_indeksa(student, broj_indeksa):
    cursor.execute("UPDATE student SET broj_indeksa=%s WHERE JSK=%s", (broj_indeksa, student.JSK))
    db.commit()
    # print(f"Promijenjen broj_indeksa studenta {student.ime} {student.prezime}.")

def update_godina_studija(student, godina_studija):
    cursor.execute("UPDATE student SET godina_studija=%s WHERE JSK=%s", (godina_studija, student.JSK))
    db.commit()
    # print(f"Promijenjena godina_studija studenta {student.ime} {student.prezime}.")

def update_status(student, status):
    cursor.execute("UPDATE student SET status=%s WHERE JSK=%s", (status, student.JSK))
    db.commit()
    # print(f"Promijenjen status studenta {student.ime} {student.prezime}.")

def update_balans(student, balans):
    cursor.execute("UPDATE student SET balans=%s WHERE JSK=%s", (balans, student.JSK))
    db.commit()
    # print(f"Promijenjen balans studenta {student.ime} {student.prezime}.")

def update_student_from_jsk_query(jsk, change, new_value):
    sql_command = f"UPDATE student SET {change}='{new_value}' WHERE jsk='{jsk}'"
    cursor.execute(sql_command)
    db.commit()
    #print(f"Promijenjena kolona: {change} u {new_value}.")
