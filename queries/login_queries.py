import os
import sys
import hashlib
sys.path.append(os.getcwd())
from database.db import cursor

m = hashlib.sha256()


def sign_in(korisnik):
    if korisnik['uloga'] == 'student':
        sql = ("select * from fakultet.student where JSK=%s")
        cursor.execute(sql, (korisnik['sifra'],))
    else:
        sql = ("select * from fakultet.sluzba where email=%s and sifra=%s")
        m.update(korisnik['sifra'].encode('utf-8'))
        cursor.execute(sql, (korisnik['email'], m.hexdigest(),))
    result = cursor.fetchone()
    return result


# korisnik = {
#     "uloga": 'student',
#     "sifra": 'jsk123'
# }
# print(sign_in(korisnik))

# korisnik1 = {
#     "uloga": 'sluzba',
#     "sifra": 'abc',
#     "email": 'sluzba.pmf@gmail.com'
# }

