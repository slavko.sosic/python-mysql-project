import os
import sys

sys.path.append(os.getcwd())
from database.db import cursor, db


def find_upis_data(year):
    sql = ("select upis_id, skolska_godina, u_toku, JSK  from fakultet.upis, fakultet.student where "
           "upis.student_id = student.student_id and year(skolska_godina) = %s")

    cursor.execute(sql, (year,))
    results = cursor.fetchall()
    if not len(results):  # moze samo if not results:
        return []
    results.insert(0, ('upis_id', 'skolska_godina', 'u_toku', 'JSK'))
    results = convert_to_bool(results)
    return results


def convert_to_bool(data_info):
    data_new = [data_info[0]]
    for data_line in data_info[1:]:
        new_item = (data_line[0], data_line[1].strftime('%d.%m.%Y'), 'false', data_line[3]) if data_line[2] == 0 else (
            data_line[0], data_line[1].strftime('%d.%m.%Y'), 'true', data_line[3])

        data_new.append(new_item)
    return data_new


def update_u_toku(data_list):
    upis_id_list = [item[0] for item in data_list]

    for id in upis_id_list:
        sql = (f"UPDATE fakultet.upis SET u_toku = 0 WHERE upis_id = {id}")
        cursor.execute(sql)
        db.commit()
    print('Sacuvane izmjene.')
