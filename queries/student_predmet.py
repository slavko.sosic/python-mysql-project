# import os
# import sys
# sys.path.append(os.getcwd())

from database.db import db, cursor
from queries.student import get_student_id, get_godina_studija


def get_nepolozeni_predmeti(student):
    cursor.reset()
    godina_studija = get_godina_studija(student)
    cursor.execute(("SELECT sp.predmet_id FROM student_predmet sp INNER JOIN predmet p ON sp.predmet_id=p.predmet_id"
                    " WHERE student_id=%s AND sp.status<>'polozen' AND p.godina_studija<=%s"),
                    (get_student_id(student), godina_studija))
    # cursor.execute(("SELECT sp.predmet_id FROM student_predmet sp INNER JOIN predmet p ON sp.predmet_id=p.predmet_id"
    #                 " WHERE student_id=%s AND sp.status<>'polozen'"),
    #                 (get_student_id(student), ))
    predmeti = cursor.fetchall() # list of tuples
    cursor.reset()
    predmeti = [predmet[0] for predmet in predmeti]
    return predmeti

def update_prijavljeni_predmeti(student, prijavljeni_predmeti):
    cursor.reset()
    student_id = get_student_id(student)
    for predmet_id in prijavljeni_predmeti:
        cursor.execute("SELECT predmet_id FROM student_predmet WHERE student_id=%s AND predmet_id=%s", (student_id, predmet_id))
        row_exists = cursor.fetchall()
        cursor.reset()
        if not row_exists:
            cursor.execute("INSERT INTO student_predmet VALUES(%s, %s, 'prijavljen')", (student_id, predmet_id))
            db.commit()

    cursor.reset()

def update_polozeni_predmeti(student, polozeni_predmeti):
    cursor.reset()
    student_id = get_student_id(student)
    for predmet_id in polozeni_predmeti:
        cursor.execute("SELECT predmet_id FROM student_predmet WHERE student_id=%s AND predmet_id=%s", (student_id, predmet_id))
        row_exists = cursor.fetchall()
        cursor.reset()
        if not row_exists:
            cursor.execute("INSERT INTO student_predmet VALUES(%s, %s, 'polozen')", (student_id, predmet_id))
            db.commit()

    cursor.reset()

def insert_student_predmet(student_id, predmet_id, status):
    cursor.reset()
    cursor.execute("INSERT INTO student_predmet VALUES(%s, %s, %s)", (student_id, predmet_id, status))
    db.commit()
    cursor.reset()
    